class PurchaseOrdersController < ApplicationController
  before_action :find_invoice, except: :destroy
  before_action :find_purchase_order, only: %w[edit update destroy]

  def index
    @purchase_orders = PurchaseOrder.all
  end

  def new
    @purchase_order = PurchaseOrder.new(purchase_order_attrs)
  end

  def create
    @purchase_order = @invoice.purchase_orders.build(purchase_order_params)

    respond_to do |format|
      if @purchase_order.save
        format.html { redirect_to purchase_orders_path, notice: I18n.t('resource.created', resource: 'Purchase Order') }
      else
        format.html { render 'new' }
      end
    end
  end

  def edit; end

  def update
    respond_to do |format|
      if @purchase_order.update(purchase_order_params)
        format.html { redirect_to purchase_orders_path, notice: I18n.t('resource.updated', resource: 'Purchase Order') }
      else
        format.html { render 'edit' }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @purchase_order.destroy
        format.html do
          redirect_to purchase_orders_path, notice: I18n.t('resource.destroyed', resource: 'Purchase Order')
        end
      else
        format.html do
          redirect_to purchase_orders_path, notice: I18n.t('resource.deleted_error', resource: 'Purchase Order')
        end
      end
    end
  end

  private

  def find_invoice
    @invoice = Invoice.find_by(id: params[:invoice_id])
  end

  def find_purchase_order
    @purchase_order = PurchaseOrder.find(params[:id])
  end

  def purchase_order_params
    params.require(:purchase_order).permit(:client_name, :amount, :tax, :vendor, :status)
  end

  def purchase_order_attrs
    return {} unless @invoice

    @invoice.attributes.slice('client_name', 'amount', 'tax')
  end
end
