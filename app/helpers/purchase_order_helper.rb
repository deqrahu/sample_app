module PurchaseOrderHelper
  def total(amount, tax)
    return (amount * (1 + (tax / 100))) if tax.present?

    amount
  end
end
